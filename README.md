to install: `npm i`
to run the generated code: `npm start`

(optional)
to build (uneedec): `npx nuxt generate`
to run a develop server: `npx nuxt`

This project uses prerendered vue.js with PWA capabilities. It utilizes tailwind css and Purge CSS to only include used styles.
